import json
import csv
import sys


def remap(key_map, data):
    for old, new in key_map.items():
        if old in data:
            data[new] = float(data.pop(old)) if old in ["total", "amount"] else str(data.pop(old))
        else:
            data[new] = 0 if new in ["AMTGROSTOT", "AMTDIST"] else ""


def remap_invoice(invoice):
    key_map = {
        "number": "IDINVC",
        "vendor": "IDVEND",
        "description": "INVCDESC",
        "date": "DATEINVC",
        "total": "AMTGROSTOT",
        "dueDate": "DATEDUE",
        "purchaseOrder": "PONBR",
    }
    remap(key_map, invoice)


def remap_line(line):
    key_map = {
        "account": "IDGLACCT",
        "amount": "AMTDIST",
        "description": "TEXTDESC",
    }
    remap(key_map, line)


def write_to_csv(file_name, data, headers):
    if not file_name.endswith(".csv"):
        file_name += ".csv"
    data_file = open(file=file_name, mode='w')
    csv_writer = csv.writer(data_file)

    csv_writer.writerow(headers)

    for item in data:
        row = []
        for header in headers:
            if header not in item:
                item[header] = ""
            row += [item[header]]
        csv_writer.writerow(row)

    data_file.close()


def main():
    args = sys.argv[1:]
    # these json objects are not alike and so we need to keep track of all unique keys encountered
    known_invoice_headers = [
        "CNTBTCH",
        "CNTITEM",
        "ORIGCOMP",
        "IDVEND",
        "IDINVC",
        "DATEINVC",
        "AMTGROSTOT",
        "ENTEREDBY",
        "DATEBUS",
        "TEXTTRX",
        "IDTRX",
        "AMTGROSDST",
        "DATEDUE",
        "INVCDESC",
        "PONBR"
    ]
    known_line_headers = ["CNTBTCH", "CNTITEM", "CNTLINE", "TEXTDESC", "AMTTOTTAX", "IDGLACCT", "AMTDIST"]
    invoice_headers = set(known_invoice_headers)
    line_headers = set(known_invoice_headers)

    file_name = args[0] if len(args) >= 1 else "data.json"
    invoice_file_name = args[1] if len(args) >= 2 else "Invoices.csv"
    invoice_details_file_name = args[2] if len(args) >= 3 else "Invoice_Details.csv"

    if not file_name.endswith(".json"):
        file_name += ".json"
    with open(file_name) as json_file:
        data = json.load(json_file)

    invoices = data.pop('nonInventoryInvoices')
    invoice_details = []
    invoice_auto_increment_id = 1

    for invoice in invoices:
        # discard the ID key?
        _ = invoice.pop('id')

        # remove lines from the header 
        lines = invoice.pop('lines')

        remap_invoice(invoice)

        # additonal data not in json
        total = invoice["AMTGROSTOT"]
        invoice['CNTITEM'] = invoice_auto_increment_id
        invoice['ORIGCOMP'] = ""
        invoice["ENTEREDBY"] = "BWSYNC"
        invoice["DATEBUS"] = ""
        invoice['CNTBTCH'] = 123
        invoice['AMTGROSDST'] = total
        invoice["TEXTTRX"] = 1 if total > 0 else 3
        invoice["IDTRX"] = 12 if total > 0 else 32

        amount_multiplier = 1 if total > 0 else -1

        new_keys = set(invoice.keys()).difference(invoice_headers)
        if len(new_keys):
            known_invoice_headers += list(new_keys)
            invoice_headers.update(list(new_keys))

        line_count = 1
        for line in lines:
            remap_line(line)
            # additonal data not in json
            line['CNTITEM'] = invoice_auto_increment_id
            line['CNTLINE'] = line_count * 20
            line['AMTTOTTAX'] = 0
            line['CNTBTCH'] = 123

            # under the assumption that a negetive total is a credit and all lines will be negetive
            line['AMTDIST'] *= amount_multiplier

            new_keys = set(line.keys()).difference(line_headers)
            if len(new_keys):
                known_line_headers += list(new_keys)
                line_headers.update(new_keys)

            line_count += 1

        invoice_details += lines
        invoice_auto_increment_id += 1

    write_to_csv(invoice_file_name, invoices, known_invoice_headers)
    write_to_csv(invoice_details_file_name, invoice_details, known_line_headers)


if __name__ == "__main__":
    main()
