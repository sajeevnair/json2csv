# Readme
This is version 1 and has almost no validation, so please be careful
## Requirements
 - Python 3 or above
 - x permision on the file
## Usage
1. python3 json2csv : this will assume that the file to read is named `data.json` and produce 2 files `Invoices.csv` and `Invoice_Details.csv`
1. python3 json2csv `filename` : will read the file with `filename.json` and produce 2 files `Invoices.csv` and `Invoice_Details.csv`
1. python3 json2csv : `filename` `invoices_filename` `invoice_details_file_name` : will read `filename.json` and produce 2 files `invoices_filename.csv` and `invoice_details_file_name.csv`

